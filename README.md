# Tips : kubectl

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectifs 
Astuces pour utiliser kubectl rapidement et efficacement, notamment pour l'examen CKA. 
Nous aborderons :

1. Les commandes impératives
2. Comment obtenir rapidement des exemples de fichiers YAML pour créer des objets
3. Comment exporter du YAML à partir d'un objet existant
4. Utiliser la documentation Kubernetes lors de la création d'objets
5. Démo


# 1. Les commandes impératives

Tout au long de ce lab, l'accent est mis principalement sur la méthode **déclarative** pour créer des objets Kubernetes. Cela consiste à définir des objets à l'aide de structures de données comme YAML ou JSON, en créant des fichiers YAML, puis en utilisant les commandes **kubectl create** ou **kubectl apply** pour créer ces objets. C'est la méthode la plus utilisée en environnement réel par les administrateurs Kubernetes.

Cependant, il existe une autre méthode pour créer des objets Kubernetes : 

- **la méthode impérative** : Cette méthode utilise des commandes et des options **kubectl** pour définir des objets, en fournissant des informations via des flags en ligne de commande plutôt que via des fichiers **YAML**. Certains trouvent les commandes impératives plus rapides. Il est conseillé d'expérimenter pour voir ce qui fonctionne le mieux. 

Pour l'examen, il peut être utile de consulter la documentation Kubernetes et d'apprendre certaines de ces commandes impératives.

Exemple de commande impérative : 

```bash
$ kubectl create deployment my-deployment -- image=nginx
```

Un **exemple de commande impérative** est **kubectl create deployment my-deployment --image=nginx**. Au lieu de créer un fichier YAML pour construire ce déploiement, tout est fait directement via la ligne de commande.

# 2. Obtenir des exemples de fichiers YAML à partir d'un objet

Même si les commandes impératives ne sont pas utilisées pour créer les objets, elles peuvent être utilisées pour obtenir rapidement des exemples de fichiers YAML. En utilisant le l'option **--dry-run**, la commande peut être exécutée sans réellement créer l'objet dans le cluster. Ce résultat peut être combiné avec le  **-o yaml** pour obtenir la sortie au format YAML.

Par exemple, pour obtenir un fichier YAML de base pour un déploiement, on peut utiliser 

```bash 
kubectl create deployment my-deployment --image=nginx --dry-run -o yaml
```

Ce fichier YAML peut ensuite être modifié avant de créer l'objet avec **kubectl create** ou **kubectl apply**.

# 3. Enregistrer une commande utilisée

Utiliser l'option **--record** permet d'enregistrer la commande utilisée pour apporter une modification sur le cluster. 

Exemple 

```bash
$ kubectl scale deployment my-deployment replicas=5 --record
```

Ensuite, en utilisant **kubectl describe** sur l'objet modifié, cette commande apparaîtra dans les annotations, ce qui est utile pour suivre les modifications apportées dans le cluster.

# 4. Utiliser la documentation Kubernetes

Lors de l'examen CKA, la documentation Kubernetes est accessible. Les articles de documentation sur les différents types d'objets, comme les déploiements, contiennent généralement des exemples YAML. Ces exemples peuvent être copiés, modifiés et utilisés pour créer des objets pendant l'examen.


# 5. Démo

[Consultez le lab relative à cette demo en suivant ce lien](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-tips-kubectl.git)

# Conclusion
Nous avons abordé les commandes impératives, comment obtenir rapidement des exemples de fichiers YAML, exporter du YAML à partir d'objets existants, et utiliser la documentation Kubernetes pour créer des objets.



# Reference 

[autocomplete :](https://kubernetes.io/docs/reference/kubectl/quick-reference/) https://kubernetes.io/docs/reference/kubectl/quick-reference/

https://kubernetes.io/docs/reference/kubectl/

https://kubernetes.io/docs/concepts/overview/working-with-objects/object-management/

https://kubernetes.io/docs/reference/kubectl/quick-reference/